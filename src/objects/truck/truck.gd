extends Node2D

var speed = 200
var active = true
@onready var brake_snd = $brake
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !active:
		return
		
	position.x += speed * delta
	if Input.is_action_just_pressed("ui_accept"):
		active = false
		Global.level += 1
		brake_snd.play()
		

func _on_visible_on_screen_notifier_2d_screen_exited():
	get_tree().change_scene_to_file("res://scenes/game_over.tscn")


func _on_brake_finished():
	get_tree().change_scene_to_file("res://scenes/loadscreen.tscn")
