extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.level > Global.backgrounds.size():
		$Label.text = "You Win!!!"
	else:
		$Label.text = "Level " + str(Global.level)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func load_level():
	if Global.level > Global.backgrounds.size():
		get_tree().change_scene_to_file("res://scenes/credits.tscn")
	else:
		get_tree().change_scene_to_file("res://scenes/main.tscn")
