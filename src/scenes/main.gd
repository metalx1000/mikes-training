extends Node2D

@onready var background = $background

# Called when the node enters the scene tree for the first time.
func _ready():

	background.texture = load(Global.backgrounds[Global.level - 1])

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
