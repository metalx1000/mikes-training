extends Control

var titlescreen = preload("res://scenes/titlescreen.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func load_title():
	var scene = titlescreen.instantiate()
	add_child(scene)
