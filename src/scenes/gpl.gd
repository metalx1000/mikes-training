extends Control

@onready var textbox = $VBoxContainer/TextEdit
# Called when the node enters the scene tree for the first time.
func _ready():
	textbox.get_v_scroll_bar().set_scale(Vector2(1.5,1))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func next():
	get_tree().change_scene_to_file("res://scenes/intro.tscn")
